package ro.ubb.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.core.model.Client;
import ro.ubb.core.service.ClientService;
import ro.ubb.web.converter.ClientConverter;
import ro.ubb.web.dto.ClientDto;
import ro.ubb.web.dto.ClientsDto;

@RestController
public class ClientController {
    public static final Logger log= LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientConverter clientConverter;

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    ClientsDto getClients(){
        log.trace("getClients -- method entered");
        ClientsDto dto = new ClientsDto(clientConverter.convertModelsToDtos((
                clientService.getAllClients()
                )));
        log.trace("getClients -- clients returned = {}", dto);
        return dto;
    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    ClientDto saveClient(@RequestBody ClientDto clientDto){
        log.trace("saveCLient -- method entered");
        ClientDto saved = clientConverter.convertModelToDto(clientService.saveClient(
                clientConverter.convertDtoToModel(clientDto)
        ));
        log.trace("saveClient -- method returned={}", saved);
        return saved;
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    ClientDto updateClient(@PathVariable Long id,
                           @RequestBody ClientDto clientDto) {
        log.trace("updateClient -- method entered");
        ClientDto updated =  clientConverter.convertModelToDto(
                clientService.updateClient(id,
                        clientConverter.convertDtoToModel(clientDto)));
        log.trace("updateClient -- method returned={}", updated);
        return updated;
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteClient(@PathVariable Long id) {
        log.trace("deleteClient -- method entered");
        clientService.deleteClient(id);

        log.trace("deleteClient -- removed client with id={}", id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.GET)
    ClientDto getClient(@PathVariable Long id) {
        log.trace("getClient -- method entered");
        ClientDto dto = clientConverter.convertModelToDto(
                clientService.getClient(id)
        );
        log.trace("getClient -- method returned={}", dto);
        return dto;
    }

    @RequestMapping(value = "/clients/name/{name}", method = RequestMethod.GET)
    ClientsDto filterClientsByName(@PathVariable String name) {
        log.trace("filterClientsByName -- method entered");
        ClientsDto dto = new ClientsDto(
                clientConverter.convertModelsToDtos(
                        clientService.filterClientsByName(name)
                )
        );
        log.trace("filterClientsByName -- method returned={}", dto);
        return dto;
    }
}
