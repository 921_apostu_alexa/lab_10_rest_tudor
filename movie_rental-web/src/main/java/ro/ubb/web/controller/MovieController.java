package ro.ubb.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.core.service.MovieService;
import ro.ubb.web.converter.MovieConverter;
import ro.ubb.web.dto.MovieDto;
import ro.ubb.web.dto.MoviesDto;

@RestController
public class MovieController {
    public static final Logger log = LoggerFactory.getLogger(MovieController.class);

    @Autowired private MovieService movieService;
    @Autowired private MovieConverter converter;

    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    MoviesDto getMovies() {
        log.trace("getMovies -- method entered");
        MoviesDto dto = new MoviesDto(converter
                .convertModelsToDtos(movieService.getAllMovies())
        );
        log.trace("getMovies -- method returned={}", dto);
        return dto;
    }

    @RequestMapping(value = "/movies", method = RequestMethod.POST)
    MovieDto saveMovie(@RequestBody MovieDto movieDto) {
        log.trace("saveMovie -- method entered");
        MovieDto dto = converter.convertModelToDto(
                movieService.saveMovie(
                        converter.convertDtoToModel(movieDto)
                )
        );
        log.trace("saveMovie -- method returned={}", dto);
        return dto;
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.PUT)
    MovieDto updateMovie(@PathVariable Long id,
                         @RequestBody MovieDto movieDto) {
        log.trace("updateMovie -- method entered");
        MovieDto dto = converter.convertModelToDto(
                movieService.updateMovie(id,
                        converter.convertDtoToModel(movieDto)
                )
        );
        log.trace("updateMovie -- method returned={}", dto);
        return dto;
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteMovie(@PathVariable Long id) {
        log.trace("deleteMovie -- method entered");
        movieService.deleteMovie(id);
        log.trace("deleteMovie -- removed movie with id={}", id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.GET)
    MovieDto getMovie(@PathVariable Long id) {
        log.trace("getMovie -- method entered");
        MovieDto dto = converter.convertModelToDto(
                movieService.getMovie(id)
        );
        log.trace("getMovie -- method returned={}", dto);
        return dto;
    }

    @RequestMapping(value = "/movies/year/{year}", method = RequestMethod.GET)
    MoviesDto filterMoviesByYear(@PathVariable Integer year) {
        log.trace("filterMoviesByYear -- method entered");
        MoviesDto dto = new MoviesDto(
                converter.convertModelsToDtos(
                        movieService.filterMoviesByYear(year)
                )
        );
        log.trace("filterMoviesByYear -- movies={}", dto);
        return dto;
    }
}
