package ro.ubb.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.core.model.Movie;
import ro.ubb.core.model.Rental;
import ro.ubb.core.service.MovieService;
import ro.ubb.core.service.RentalService;
import ro.ubb.web.converter.MovieConverter;
import ro.ubb.web.converter.RentalConverter;
import ro.ubb.web.dto.MovieDto;
import ro.ubb.web.dto.RentalDto;
import ro.ubb.web.dto.RentalsDto;

@RestController
public class RentalController {
    public static final Logger log = LoggerFactory.getLogger(RentalController.class);

    @Autowired private RentalService service;
    @Autowired private MovieService  movieService;

    @Autowired private RentalConverter converter;
    @Autowired private MovieConverter  movieConverter;

    @RequestMapping(value = "/rentals", method = RequestMethod.GET)
    RentalsDto getRentals() {
        log.trace("getRentals -- method entered");
        RentalsDto dto = new RentalsDto(converter
                .convertModelsToDtos(service.getAllRentals())
        );
        log.trace("getRentals -- method returned={}", dto);
        return dto;
    }

    @RequestMapping(value = "/rentals", method = RequestMethod.POST)
    RentalDto saveRental(@RequestBody RentalDto rentalDto) {
        log.trace("saveRental -- method entered");
        Rental rental = converter.convertDtoToModel(rentalDto);
        RentalDto dto = converter.convertModelToDto(
                service.rent(
                        rental.getClientId(),
                        rental.getMovieId()
                )
        );
        log.trace("saveRental -- method returned={}", dto);
        return dto;
    }

    @RequestMapping(value = "/rentals/{id}", method = RequestMethod.PUT)
    RentalDto updateRental(@PathVariable Long id,
                           @RequestBody RentalDto rentalDto) {
        log.trace("updateRental -- method entered");
        RentalDto dto = converter.convertModelToDto(
                service.updateRental(id,
                        converter.convertDtoToModel(rentalDto)
                )
        );
        log.trace("updateRental -- method returned={}", dto);
        return dto;
    }

    @RequestMapping(value = "/rentals/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteRental(@PathVariable Long id) {
        log.trace("deleteRental -- method entered");
        service.deleteRental(id);
        log.trace("deleteRental -- removed rental with id={}", id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/rentals/{id}", method = RequestMethod.GET)
    RentalDto getRental(@PathVariable Long id) {
        log.trace("getRental -- method entered");
        RentalDto dto = converter.convertModelToDto(
                service.getRental(id)
        );
        log.trace("getRental -- method returned={}", dto);
        return dto;
    }

    @RequestMapping(value = "/rentals/clients/{clientId}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteRentalsWithClientId(@PathVariable Long clientId) {
        log.trace("deleteRentalsWithClientId -- method entered");
        service.removeRentalsWithClientID(clientId);
        log.trace("deleteRentalsWithClientId -- removed rental with clientId={}", clientId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/rentals/movies/{movieId}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteRentalsWithMovieId(@PathVariable Long movieId) {
        log.trace("deleteRentalsWithMovieId -- method entered");
        service.removeRentalsWithClientID(movieId);
        log.trace("deleteRentalsWithMovieId -- removed rental with movieId={}", movieId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/rentals/stats", method = RequestMethod.GET)
    MovieDto getMostRentedMovie() {
        log.trace("getMostRentedMovie -- method entered");
        Long movieId = service.getMostRentedMovieId();
        Movie movie = movieService.getMovie(movieId);
        log.trace("getMostRentedMovie -- method returned={}", movie);
        return movieConverter.convertModelToDto(movie);
    }
}