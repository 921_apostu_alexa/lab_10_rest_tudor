package ro.ubb.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ro.ubb.core.model.BaseClass;

import java.io.Serializable;


@NoRepositoryBean
public interface IRepository<T extends BaseClass<ID>,ID extends Serializable>
        extends JpaRepository<T, ID> {
}
