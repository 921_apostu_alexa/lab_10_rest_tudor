package ro.ubb.core.repository;

import ro.ubb.core.model.Movie;

public interface MovieRepository extends IRepository<Movie, Long> {
}
