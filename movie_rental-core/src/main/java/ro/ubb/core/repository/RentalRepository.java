package ro.ubb.core.repository;

import ro.ubb.core.model.Rental;

public interface RentalRepository extends IRepository<Rental, Long> {
}
