package ro.ubb.core.model;
import lombok.*;

import javax.persistence.Entity;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Rental extends BaseClass<Long> {
    private Long clientId;
    private Long movieId;
    private Date rentDate;
}
