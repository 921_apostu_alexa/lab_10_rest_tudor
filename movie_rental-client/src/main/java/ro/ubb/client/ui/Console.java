package ro.ubb.client.ui;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ro.ubb.web.dto.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class Console {
    public static final String BaseURL = "http://localhost:8082/api/";
    public static final String ClientUrl = BaseURL + "clients";
    public static final String MovieUrl = BaseURL + "movies";
    public static final String RentalUrl = BaseURL + "rentals";

    @Autowired
    private RestTemplate restTemplate;

    private Menu menu;
    private boolean running = true;

    public void initialiseMenu() {
        this.menu = new Menu();
        this.menu.addCommand("c1", "Add client", this::addClient);
        this.menu.addCommand("c2", "Print all clients", this::printClients);
        this.menu.addCommand("c3", "Filter clients by name", this::filterClients);
        this.menu.addCommand("c4", "Update client", this::updateClient);
        this.menu.addCommand("c5", "Remove client", this::removeClient);
        this.menu.addCommand("", "", null);
        this.menu.addCommand("m1", "Add movie", this::addMovie);
        this.menu.addCommand("m2", "Print all movies", this::printMovies);
        this.menu.addCommand("m3", "Filter movies by year", this::filterMovies);
        this.menu.addCommand("m4", "Update movie", this::updateMovie);
        this.menu.addCommand("m5", "Remove movie", this::removeMovie);
        this.menu.addCommand("", "", null);
        this.menu.addCommand("r1", "Add rental", this::addRental);
        this.menu.addCommand("r2", "Print all rentals", this::printRentals);
        this.menu.addCommand("r3", "Print the most rented movie", this::printMostRentedMovie);
        this.menu.addCommand("r4", "Update rental", this::updateRental);
        this.menu.addCommand("r5", "Remove rental", this::removeRental);
        this.menu.addCommand("", "", null);
        this.menu.addCommand("x", "Exit", () -> this.running = false);
    }

    public void run() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(this.menu.getMenu());
        try {
            System.out.println("\n>");
            String choice = buffer.readLine();
            this.menu.run(choice);

            if (this.running)
                this.run();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }


    private ClientDto readClient() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter client name: ");
            String name = buffer.readLine();

            System.out.println("Enter client email: ");
            String email = buffer.readLine();

            return new ClientDto(name, email);

        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        return null;
    }

    private MovieDto readMovie() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter movie title: ");
            String title = buffer.readLine();

            System.out.println("Enter movie duration (in minutes): ");
            int minutes = Integer.parseInt(buffer.readLine());

            System.out.println("Enter movie year: ");
            int year = Integer.parseInt(buffer.readLine());

            return new MovieDto(title, minutes, year);

        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        return null;
    }


    public void addClient() {
        System.out.println(" -- Adding client -- ");
        ClientDto client = this.readClient();

        ClientDto savedClient = restTemplate
                .postForObject(ClientUrl, client, ClientDto.class);
        System.out.println("Saved client " + savedClient);
    }

    public void printClients() {
        System.out.println(" << Clients >> ");
        ClientsDto clients = restTemplate.getForObject(ClientUrl, ClientsDto.class);

        if (clients != null)
            clients.getClients().forEach(System.out::println);
        System.out.println();
    }

    public void filterClients() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Filtering clients -- ");
            System.out.println("Enter client name: ");
            String name = buffer.readLine();

            ClientsDto clients = restTemplate
                    .getForObject(ClientUrl + "/name/{name}", ClientsDto.class, name);

            if (clients != null)
                clients.getClients().forEach(System.out::println);
            System.out.println();
        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    public void updateClient() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Updating client -- ");

            System.out.println("Enter client id:");
            Long id = Long.parseLong(buffer.readLine());

            System.out.println("Enter client name: ");
            String name = buffer.readLine();

            System.out.println("Enter client email: ");
            String email = buffer.readLine();

            ClientDto client = new ClientDto(name, email);

            restTemplate.put(ClientUrl + "/{id}", client, id);

            client.setId(id);
            System.out.println("Updated client " + client);
        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    public void removeClient() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Removing client -- ");
            System.out.println("Enter client id: ");
            Long id = Long.parseLong(buffer.readLine());

            restTemplate.delete(ClientUrl + "/{id}", id);
            restTemplate.delete(RentalUrl + "/clients/{clientId}", id);

            System.out.println("Removed client with id " + id);
        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }


    public void addMovie() {
        System.out.println(" -- Adding movie -- ");
        MovieDto movie = readMovie();

        MovieDto savedMovie = restTemplate
                .postForObject(MovieUrl, movie, MovieDto.class);
        System.out.println("Saved movie " + savedMovie);
    }

    public void printMovies() {
        System.out.println(" << Movies >> ");
        MoviesDto movies = restTemplate.getForObject(MovieUrl, MoviesDto.class);

        if (movies != null)
            movies.getMovies().forEach(System.out::println);
        System.out.println();
    }

    public void filterMovies() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Filtering movies -- ");
            System.out.println("Enter movie year: ");
            int year = Integer.parseInt(buffer.readLine());

            MoviesDto movies = restTemplate
                    .getForObject(MovieUrl + "/year/{year}", MoviesDto.class, year);

            if (movies != null)
                movies.getMovies().forEach(System.out::println);
            System.out.println();
        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    public void updateMovie() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Updating movie -- ");

            System.out.println("Enter movie id:");
            Long id = Long.parseLong(buffer.readLine());

            System.out.println("Enter movie title: ");
            String title = buffer.readLine();

            System.out.println("Enter movie duration (in minutes): ");
            int minutes = Integer.parseInt(buffer.readLine());

            System.out.println("Enter movie year: ");
            int year = Integer.parseInt(buffer.readLine());

            MovieDto movie = new MovieDto(title, minutes, year);

            restTemplate.put(MovieUrl + "/{id}", movie, id);

            movie.setId(id);
            System.out.println("Updated movie " + movie);
        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    public void removeMovie() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Removing movie -- ");
            System.out.println("Enter movie id: ");
            Long id = Long.parseLong(buffer.readLine());

            restTemplate.delete(MovieUrl + "/{id}", id);
            restTemplate.delete(RentalUrl + "/movies/{movieId}", id);

            System.out.println("Removed movie with id " + id);
        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }


    public void addRental() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter client id: ");
            Long clientId = Long.parseLong(buffer.readLine());

            System.out.println("Enter movie id: ");
            Long movieId = Long.parseLong(buffer.readLine());

            RentalDto rental = new RentalDto(clientId, movieId, new Date());
            RentalDto savedRental = restTemplate
                    .postForObject(RentalUrl, rental, RentalDto.class);
            System.out.println("Saved rental " + savedRental);
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    public void printRentals() {
        System.out.println(" << Rentals >> ");
        RentalsDto rentals = restTemplate.getForObject(RentalUrl, RentalsDto.class);

        if (rentals != null)
            rentals.getRentals().forEach(System.out::println);
        System.out.println();
    }

    public void printMostRentedMovie() {
        System.out.println(" << Most Rented Movie >>");
        MovieDto movie = restTemplate.getForObject(RentalUrl + "/stats", MovieDto.class);
        System.out.println(movie);
    }

    public void updateRental() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Updating rental -- ");

            System.out.println("Enter rental id: ");
            Long id = Long.parseLong(buffer.readLine());

            System.out.println("Enter rental's client id: ");
            Long clientId = Long.parseLong(buffer.readLine());

            System.out.println("Enter rental's movie id: ");
            Long movieId = Long.parseLong(buffer.readLine());

            System.out.println("Enter rental's date: ");
            Date date = new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(buffer.readLine());

            RentalDto rental = new RentalDto(clientId, movieId, date);

            restTemplate.put(RentalUrl + "/{id}", rental, id);

            rental.setId(id);
            System.out.println("Updated rental " + rental);
        } catch(IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    public void removeRental() {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(" -- Removing rental -- ");
            System.out.println("Enter rental id: ");
            Long id = Long.parseLong(buffer.readLine());

            restTemplate.delete(RentalUrl + "/{id}", id);

            System.out.println("Removed rental with id " + id);
        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }
}
