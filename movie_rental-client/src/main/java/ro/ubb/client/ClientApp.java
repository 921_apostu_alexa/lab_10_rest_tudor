package ro.ubb.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ro.ubb.client.ui.Console;

public class ClientApp {
    public static void main(String[] args) {

        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        "ro.ubb.client.config"
                );


        Console console = context.getBean(Console.class);

        try {
            console.initialiseMenu();
            console.run();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}